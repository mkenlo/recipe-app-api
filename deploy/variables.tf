variable "prefix" {
  default     = "raad"
  description = "abbreviation of Recipe App Api Devops"
}

variable "project" {
  default = "recipe-app-api"
}

variable "contact" {
  default = "mkenlo@brainspark.me"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"

}

variable "db_password" {
  description = "Password for the RDS Postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "531020356164.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "531020356164.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
